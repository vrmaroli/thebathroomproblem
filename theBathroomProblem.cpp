/*	The bathroom problem
 *	Author: Vishnu Ramesh Maroli
 */

#ifndef _REENTRANT
#define _REENTRANT
#endif

#include <iostream>
#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>
#include <cstdlib>
#include <ctime>

#define NUMBER_OF_KIDS 10
bool uselessPrint = true;

using namespace std;

void* bathroomUserMale(void* arg);
void* bathroomUserFemale(void* arg);

int femalesWaitingCount = 0, malesWaitingCount = 0;
//pthread_mutex_t countUpdate_waiting;

int malesInBathroom = 0, femalesInBathroom = 0;
//pthread_mutex_t countUpdate_bathroom;

sem_t maleBathroom, femaleBathroom, mutex;
sem_t systemOut;

int main(int argc, char* argv[]) {
	srand (time(NULL));
	pthread_t threads[NUMBER_OF_KIDS];
	sem_init(&maleBathroom, 0, 0);
	sem_init(&femaleBathroom, 0, 0);
	sem_init(&mutex, 0, 1);
	sem_init(&systemOut, 0, 1);

	for(int i = 0; i < NUMBER_OF_KIDS; i++) {
		int *arg = new int;
		*arg = i;
		// Sex determined at birth
		if(rand()%2) {
			pthread_create(&threads[i], NULL, &bathroomUserMale, (void*)arg);
		}
		else {
			pthread_create(&threads[i], NULL, &bathroomUserFemale, (void*)arg);
		}
	}
	for(int i = 0; i < NUMBER_OF_KIDS; i++) {
		pthread_join(threads[i], NULL);
	}
	return 0;
}

void* bathroomUserMale(void* arg) {
	int ID = *((int *) arg);
	free(arg);

	if(uselessPrint) {
		sem_wait(&systemOut);
		cout << "Kid #" << ID << " born: Its a Boy" << endl ;
		sem_post(&systemOut);
	}

	int i = 10;
	while(i--) {
		usleep((rand()%10 + 1) * 10000);	// sleeping

		if(uselessPrint) {
			sem_wait(&systemOut);
			cout << ID << "(Boy) starts waiting" << endl ;
			sem_post(&systemOut);
		}

		sem_wait(&mutex);
		if(femalesInBathroom > 0) {
			malesWaitingCount++;
			sem_post(&mutex);
			sem_wait(&maleBathroom);
			malesWaitingCount--;
		}
		malesInBathroom++;
		sem_wait(&systemOut);
		cout << "Males:Females waiting[inside]\t" << malesWaitingCount << " [" << malesInBathroom << "]:" << femalesWaitingCount << " [" << femalesInBathroom << "]" << endl ;
		sem_post(&systemOut);
		if(malesWaitingCount>0)
			sem_post(&maleBathroom);
		else
			sem_post(&mutex);

		if(uselessPrint) {
			sem_wait(&systemOut);
			cout << ID << "(Boy) gets resource" << endl ;
			sem_post(&systemOut);
		}
		usleep((rand()%5 + 1) * 10000); 	// using the bathroom, Finally ...
		
		sem_wait(&mutex);
		malesInBathroom--;
		if(malesInBathroom==0 and femalesWaitingCount>0)
			sem_post(&femaleBathroom);
		else
			sem_post(&mutex);

		if(uselessPrint) {
			sem_wait(&systemOut);
			cout << ID << "(Boy) finished" << endl ;
			sem_post(&systemOut);
		}
	}

	pthread_exit(0);
}

void* bathroomUserFemale(void* arg) {
	int ID = *((int *) arg);
	free(arg);

	if(uselessPrint) {
		sem_wait(&systemOut);
		cout << "Kid #" << ID << " born: Its a Gal" << endl ;
		sem_post(&systemOut);
	}

	int i = 10;

	while(i--) {
		usleep((rand()%10 + 1) * 10000);	// sleeping

		if(uselessPrint) {
			sem_wait(&systemOut);
			cout << ID << "(Gal) starts waiting" << endl ;
			sem_post(&systemOut);
		}

		sem_wait(&mutex);
		if(malesInBathroom > 0) {
			femalesWaitingCount++;
			sem_post(&mutex);
			sem_wait(&femaleBathroom);
			femalesWaitingCount--;
		}
		femalesInBathroom++;
		sem_wait(&systemOut);
		cout << "Males:Females waiting[inside]\t" << malesWaitingCount << " [" << malesInBathroom << "]:" << femalesWaitingCount << " [" << femalesInBathroom << "]" << endl ;
		sem_post(&systemOut);
		if(femalesWaitingCount>0)
			sem_post(&femaleBathroom);
		else
			sem_post(&mutex);

		if(uselessPrint) {
			sem_wait(&systemOut);
			cout << ID << "(Gal) gets resource" << endl ;
			sem_post(&systemOut);
		}
		usleep((rand()%5 + 1) * 10000); 	// using the bathroom, Finally ...
		
		sem_wait(&mutex);
		femalesInBathroom--;
		if(femalesInBathroom==0 and malesWaitingCount>0)
			sem_post(&maleBathroom);
		else
			sem_post(&mutex);

		if(uselessPrint) {
			sem_wait(&systemOut);
			cout << ID << "(Gal) finished" << endl ;
			sem_post(&systemOut);
		}
	}

	pthread_exit(0);
}